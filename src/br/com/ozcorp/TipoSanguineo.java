/**
 * @author Luiza Fagundes
 */
package br.com.ozcorp;

public enum TipoSanguineo {

	// Op��es para informar no funcion�rio
	APOSITIVO("A+"), ANEGATIVO("A-"), BPOSITIVO("B+"), BNEGATIVO(
			"B-"), OPOSITIVO("O+"), ONEGATIVO(
					"O-"), ABPOSITIVO("AB+"), ABNEGATIVO("AB-");

	public String sangue;

	TipoSanguineo(String sangue) {
		this.sangue = sangue;
	}

}
