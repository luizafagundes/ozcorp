/**
 * @author Luiza Fagundes
 */
package br.com.ozcorp;

public class FuncionarioTestDrive {

	public static void main(String[] args) {

		// instancia um funcionário
		Analista analista = new Analista("Giovanna", "38.113.043-5", "363.150.148-01", "1250", "gigi.diodato@hotmail.com",
				"21125", TipoSanguineo.ABNEGATIVO, Sexo.FEMININO, 4,
				new Departamento("Financeiro", new Cargo("Analista", 5_000)));

		// imprime funcionario
		System.out.println("Dados do Funcionário: ");
		System.out.println("Nome:            " + analista.getNome());
		System.out.println("RG:              " + analista.getRg());
		System.out.println("CPF:             " + analista.getCpf());
		System.out.println("Matrícula:       " + analista.getMatricula());
		System.out.println("Email:           " + analista.getEmail());
		System.out.println("Senha:           " + analista.getSenha());
		System.out.println("Departamento:    " + analista.getDepartamento().getNome());
		System.out.println("Cargo:           " + analista.getDepartamento().getCargo().getTitulo());
		System.out.println("Sálario Base:    " + analista.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Nível de Acesso: " + analista.getNivelAcesso());
		System.out.println("Tipo Sanguíneo:  " + analista.getTipoSanguineo().sangue);
		System.out.println("Sexo:            " + analista.getSexo().sexo);

	}
}