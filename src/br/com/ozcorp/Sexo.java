/**
 * @author Luiza Fagundes
 */
package br.com.ozcorp;

	public enum Sexo {
		
		// Op��es para informar no funcion�rio
		MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
		
		public String sexo;
		
		Sexo(String sexo) {
			this.sexo = sexo;
		}
	}

