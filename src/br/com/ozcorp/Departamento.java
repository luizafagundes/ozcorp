/**
 * @author Luiza Fagundes
 */
package br.com.ozcorp;

public class Departamento {

	// ATRIBUTOS
	private String nome;
	private Cargo cargo;

	// CONSTRUTOR
	public Departamento(String nome, Cargo cargo) {
		this.nome = nome;
		this.cargo = cargo;
	}

	// GETTERS && SETTERS
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

}
